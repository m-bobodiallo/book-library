/* tslint:disable:no-redundant-jsdoc */
import { Injectable } from '@angular/core';
import {Book} from '../models/book.model';
import {Subject} from 'rxjs';
import firebase from 'firebase';
import DataSnapshot = firebase.database.DataSnapshot;

@Injectable({
  providedIn: 'root'
})
export class BooksService {
  books: Book[] = [];
  booksSubject = new Subject<Book[]>();

  constructor() {
    this.getBooks();
  }

  /**
   * Refresh books for another component
   * @return void
   */
  emitBooks(): void {
    this.booksSubject.next(this.books);
  }

  /**
   * Save the books (use put method HTTP)
   * @return void
   */
  saveBook(): void {
    firebase.database().ref('/books').set(this.books);
  }

  /**
   * Get List of books
   * @return void
   */
  getBooks(): void {
    firebase.database().ref('/books')
      .on('value', (data: DataSnapshot) => {
        this.books = data.val() ? data.val() : [];
        this.emitBooks();
      });
  }

  /**
   * Get book by id
   * @param id
   * @return Promise<any>
   */
  getSingleBook(id: number): Promise<any> {
    return new Promise(
      (resolve, reject) => {
        firebase.database().ref('/books/' + id).once('value').then(
          (data: DataSnapshot) => {
            resolve(data.val());
          }, (error) => {
            reject(error);
          }
        );
      }
    );
  }

  createNewBook(newBook: Book): void {
    this.books.push(newBook);
    this.saveBook();
    this.emitBooks();
  }

  removeBook(book: Book): void {
    const bookIndexToRemove = this.books.findIndex(
      (bookEl) => {
        if (bookEl === book) {
          return true;
        }
      }
    );
    this.books.splice(bookIndexToRemove, 1);
    this.saveBook();
    this.emitBooks();
  }
}
