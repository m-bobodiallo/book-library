import { Component } from '@angular/core';
import firebase from 'firebase';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'book-library';

  constructor() {
    const firebaseConfig = {
      apiKey: 'AIzaSyDZYMU_GCz9DnndqU0Li8V-l2UhQDycDjc',
      authDomain: 'book-library-ff7a2.firebaseapp.com',
      projectId: 'book-library-ff7a2',
      storageBucket: 'book-library-ff7a2.appspot.com',
      messagingSenderId: '843337989511',
      appId: '1:843337989511:web:2cf8d32e5d8dac00cba722'
    };
    firebase.initializeApp(firebaseConfig);
  }
}
